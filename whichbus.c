/*
   CITS2002 Project 1 2015
   Name(s):		        Eric Goodwin
   Student number(s):	21488108
   Date:		        18-09-2015
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

#define MAX_FIELD_LEN                   100
#define MAX_MINS_BEFORE_FIRST_RIDE      60
#define MAX_WALKING_METRES              1000
#define WALKING_METRES_PER_SEC          1
#define METRES_TO_MINS(m)               (((m)/WALKING_METRES_PER_SEC)/60 + 1)
#define pi 3.145926535898

// Store bus/train stop details.
struct Stop {
  char name[MAX_FIELD_LEN];
  int id;
  int stn_id;
  double lat;
  double longt;
  int is_null;
};

// Handle times.
struct Time {
  int hour;
  int mins;
};

typedef struct Stop stop;
typedef struct Time time;

const char *FILES_DIR;
time CUR_TIME;

void find_closest_stops(stop *buf, double start[], double end[]);
void get_next_service(int stop_id, time arrive_time, int *trip_id, time *depart_time);
void trim_line(char *line);
void increase_time(time *incr, int mins);
void char_to_time(time *container, char *time);
int time_is_greater(time a, time b);
int time_delta(time a, time b);
int get_route_id(int trip_id);
int distance(double start_lat, double start_longt, double end_lat, double end_longt);
double deg2rad(double d);
double rad2deg(double r);
char *get_bus_num(int route_id);

int main(int argc, char *argv[]) {
 // Get environment var.
  char *date = getenv("LEAVEHOME");
  FILES_DIR = argv[1];

  // Get time from input, only time is important now.
  char *time_from_env = &date[4];
  // Set minutes (and convert to int).
  CUR_TIME.mins = (int) strtol(&time_from_env[3], NULL, 10);
  // Remove minutes and colon from time string.
  time_from_env[strlen(time_from_env)-3] = 0;
  // Set hour (and convert to int).
  CUR_TIME.hour = (int) strtol(&time_from_env[0], NULL, 10);

  double start_lat = strtod(argv[2], NULL);
  double start_long = strtod(argv[3], NULL);
  double end_lat = strtod(argv[4], NULL);
  double end_long = strtod(argv[5], NULL);

  // Sanity check input.
  if (start_lat == end_lat && start_long == end_long) {
    printf("Invalid input, start and end coords are the same!\n");
    exit(EXIT_FAILURE);
  }

  double start[] = {start_lat, start_long};
  double end[] = {end_lat, end_long};

  stop bus_stops[(sizeof(stop) * 2) +1];
  find_closest_stops(bus_stops, start, end);

  // Distance to start bus stop.
  int dist_to_stop = distance(start_lat, start_long, bus_stops[0].lat, bus_stops[0].longt);
  // Distance to final destination from arrival bus stop.
  int dist_to_dest = distance(end_lat, end_long, bus_stops[1].lat, bus_stops[1].longt);

  if (dist_to_stop > MAX_WALKING_METRES || dist_to_dest > MAX_WALKING_METRES) {
    printf("%d:%d not possible\n", CUR_TIME.hour, CUR_TIME.mins);
    printf("You would have to walk more than 1000m to reach the bus stop or destination.\n");
  } else {
    printf("%d:%d walk %dm to stop %d %s\n", CUR_TIME.hour, CUR_TIME.mins, dist_to_stop, bus_stops[0].id, bus_stops[0].name);

    // Add walking time to CUR_TIME.
    increase_time(&CUR_TIME, METRES_TO_MINS(dist_to_stop));

    // Get bus/train details.
    int trip_id;
    time depart_time;
    get_next_service(bus_stops[0].id, CUR_TIME, &trip_id, &depart_time);

    int route_id = get_route_id(trip_id);
    if (route_id > 0) {
      char *bus_no = get_bus_num(route_id);

      // Update CUR_TIME to departure time of bus.
      CUR_TIME.hour = depart_time.hour;
      CUR_TIME.mins = depart_time.mins;

      printf("%d:%d catch bus %s to stop %d %s\n", CUR_TIME.hour, CUR_TIME.mins, bus_no, bus_stops[1].id, bus_stops[1].name);

      time arrival_time;
      get_next_service(bus_stops[1].id, CUR_TIME, &trip_id, &arrival_time);

      // Update CUR_TIME to departure time of bus.
      CUR_TIME.hour = arrival_time.hour;
      CUR_TIME.mins = arrival_time.mins;

      printf("%d:%d walk %dm to destination\n", CUR_TIME.hour, CUR_TIME.mins, dist_to_dest);

      // Add walking time to CUR_TIME.
      increase_time(&CUR_TIME, METRES_TO_MINS(dist_to_dest));

      printf("%d:%d arrive\n", CUR_TIME.hour, CUR_TIME.mins);
      exit(EXIT_SUCCESS);

    } else {
      printf("An error occured getting the route id!\n");
      exit(EXIT_FAILURE);
    }
  }

  return 0;
}

/**
 * Read stops.txt and find closest stop to start and end locations.
 * This function finds both the start and end stops so the stops.txt file
 * only needs to be read once.
 */
void find_closest_stops(stop *buf, double start[], double end[]) {
  stop start_stop, end_stop, current_stop;
  char const *stops_filename = "/stops.txt";
  start_stop.is_null = 1;
  end_stop.is_null = 1;

  char line[1024];
  // Determine path to stops.txt
  char *file_path = malloc(strlen(FILES_DIR) + strlen(stops_filename) + 1);
  strcat(strcpy(file_path, FILES_DIR), stops_filename);
  FILE *stops = fopen(file_path, "r");

  if (stops != NULL) {
    int count = 0;
    while (fgets(line, sizeof(line), stops) != NULL) {
      // Skip first row.
      if (count > 0) {
        char *cur_line = line;
        trim_line(cur_line);
        char *field;
        const char delim[2] = ",";

        int field_count = 0;
        // Go through each field of the row.
        while ((field = strsep(&cur_line, delim)) != NULL ) {
          switch(field_count) {
            case 1: {
              // Check if this stop is a train station.
              if (strlen(field) > 0) {
                current_stop.stn_id = (int) strtol(field, NULL, 10);
              }
              break;
            }

            case 3:
              current_stop.id = (int) strtol(field, NULL, 10);
              break;

            case 4:
              strcpy(current_stop.name, field);
              break;

            case 6:
              current_stop.lat = strtod(field, NULL);
              break;

            case 7:
             current_stop.longt = strtod(field, NULL);
             break;
          }

          field_count++;
        }

        // Check if this stop is closest to start or end location.
        if (start_stop.is_null || (distance(start[0], start[1], current_stop.lat, current_stop.longt) < distance(start[0], start[1], start_stop.lat, start_stop.longt))) {
          // Update start_stop with new lowest distance.
          strcpy(start_stop.name, current_stop.name);
          start_stop.lat = current_stop.lat;
          start_stop.longt = current_stop.longt;
          start_stop.id = current_stop.id;
          start_stop.is_null = 0;
        }

        if (end_stop.is_null || (distance(end[0], end[1], current_stop.lat, current_stop.longt) < distance(end[0], end[1], end_stop.lat, end_stop.longt))) {
          strcpy(end_stop.name, current_stop.name);
          end_stop.lat = current_stop.lat;
          end_stop.longt = current_stop.longt;
          end_stop.id = current_stop.id;
          end_stop.is_null = 0;
        }
      }

      count++;
    }
  } else {
    printf("Unable to read stops.txt!\n");
    exit(EXIT_FAILURE);
  }

  if (stops != NULL) {
    fclose(stops);
  }
  free(file_path);

  buf[0] = start_stop;
  buf[1] = end_stop;
}

/**
 * Use stop_times.txt to get the next trip based on stop_id.
 */
void get_next_service(int stop_id, time arrive_time, int *trip_id, time *depart_time) {
  char const *stops_filename = "/stop_times.txt";

  char line[1024];
  // Determine path to stops_times.txt
  char *file_path = malloc(strlen(FILES_DIR) + strlen(stops_filename) + 1);
  strcat(strcpy(file_path, FILES_DIR), stops_filename);
  FILE *stop_times = fopen(file_path, "r");

  if (stop_times != NULL) {
    int count = 0;

    while (fgets(line, sizeof(line), stop_times) != NULL) {
      int field_count = 0;
      char *cur_line = line;
      trim_line(cur_line);
      int temp_trip_id;
      time temp_depart_time;
      char *field;
      const char delim[2] = ",";

      if (count > 0) {
        while ((field = strsep(&cur_line, delim)) != NULL ) {

          switch (field_count) {
            case 0:
              temp_trip_id = (int) strtol(field, NULL, 10);
              break;

            case 2:
              char_to_time(&temp_depart_time, field);
              break;

            case 3: {
              if (strtol(field, NULL, 10) == stop_id) {
                // Make sure the bus is leaving after we arrive at the stop.
                int time_check = time_is_greater(temp_depart_time, arrive_time);
                int wait_time = time_delta(temp_depart_time, arrive_time);

                if ((time_check == 1 || time_check == -1) && wait_time > 0 && (wait_time <= MAX_MINS_BEFORE_FIRST_RIDE)) {
                  memcpy(trip_id, (void *)&temp_trip_id, sizeof(temp_trip_id));
                  memcpy(depart_time, (void *)&temp_depart_time, sizeof(temp_depart_time));
                  fclose(stop_times);
                  return;
                }
              }
            }
          }

          field_count++;
        }
      }

      count++;
    }

  } else {
    printf("Unable to read stop_times.txt!\n");
    exit(EXIT_FAILURE);
  }

  if (stop_times != NULL) {
    fclose(stop_times);
  }

  free(file_path);

  printf("%d:%d not possible\n", CUR_TIME.hour, CUR_TIME.mins);
  printf("Next service doesn't arrive for over one hour.\n");
  exit(EXIT_FAILURE);
}

/**
 * Remove line ends and replaced with null-byte
 * Adapted from code provided in lecture.
 *
 * @param char[] line
 *  The line to trim.
 */
void trim_line(char line[]) {
  int i = 0;

  while (line[i] != '\0') {
    if (line[i] == '\r' || line[i] == '\n') {
      line[i] = '\0';
      break;
    }
    i++;
  }
}

/**
 * Calculate the distance between two lat/long pairs (in km).
 *
 * @param double start_lat
 *  Starting lattitude.
 * @param double start_longt
 *  Starting longtitude.
 * @param double end_lat
 *  End lattitude.
 * @param double end_longt
 *  End Longtitude.
 *
 * @return int
 *  distance between the two points in METRES.
 */
int distance(double start_lat, double start_longt, double end_lat, double end_longt) {
  double x, dist;
  x = start_longt - end_longt;
  dist = sin(deg2rad(start_lat)) * sin(deg2rad(end_lat)) + cos(deg2rad(start_lat)) * cos(deg2rad(end_lat)) * cos(deg2rad(x));
  dist = acos(dist);
  dist = rad2deg(dist);
  // * 1000 is to convert to metres.
  dist = (int) (dist * 60 * 1.1515 * 1.609344 * 1000);

  return dist;
}

/**
 * Convert degress to radians.
 */
double deg2rad(double d) {
  return (d * pi / 180);
}

/**
 * Convert radians to degrees.
 */
double rad2deg(double r) {
  return (r * 180 / pi);
}

/**
 * Increment a time struct by the set mins.
 *
 * @param int mins
 *   The number of mins to increment the time by.
 */
void increase_time(time *incr, int mins) {
  if (mins > 60) {
    while (mins > 60) {
      mins -= 60;
      // If the hour > 24, roll back to 0.
      if ((incr->hour) + 1 > 24) {
        incr->hour = 0;
      } else {
        incr->hour++;
      }
    }
  }

  if ((incr->mins + mins) > 60) {
    int remaining_mins = incr->mins + mins;

    do {
      if ((incr->hour) + 1 > 24) {
        incr->hour = 0;
      } else {
        incr->hour++;
      }
      remaining_mins -= 60;

    } while (remaining_mins > 60);
  } else {
    incr->mins += mins;
  }
}

/**
 * Read a time string and convert it to a time struct.
 *
 * @param time container
 *  Time struct to assign times to.
 * @param char time
 *  String to read time from.
 */
void char_to_time(time *container, char *time) {
  char *new_time = &time[0];
  container->mins = (int) strtol(&new_time[3], NULL, 10);
  new_time[strlen(new_time)-3] = 0;
  container->hour = (int) strtol(&new_time[0], NULL, 10);
}

/**
 * Compare two times to determine if a is greater than b.
 *
 * @param time a
 *  Time a
 * @param time b
 *  Time b
 *
 * @return int
 *  1 if a > b, 0 if b > a and -1 if they are equal.
 */
int time_is_greater(time a, time b) {
  // First check hours.
  if (a.hour > b.hour) {
    return 1;
  } else if ((a.hour == b.hour) && (a.mins > b.mins)) {
    return 1;
  } else if ((a.hour == b.hour) && (a.mins == b.mins)) {
    return -1;
  }

  return 0;
}

/**
 * Get the difference between two times in minutes.
 */
int time_delta(time a, time b) {
  int a_mins = (a.hour * 60) + a.mins;
  int b_mins = (b.hour * 60) + b.mins;

  return a_mins - b_mins;
}

/**
 * Get bus number corresponding to trip_id.
 */
int get_route_id(int trip_id) {
  // Get route_id.
  char const *trips_filename = "/trips.txt";

    char line[1024];
    // Determine path to stops_times.txt
    char *file_path = malloc(strlen(FILES_DIR) + strlen(trips_filename) + 1);
    strcat(strcpy(file_path, FILES_DIR), trips_filename);
    FILE *trips = fopen(file_path, "r");

    if (trips != NULL) {
      int count = 0;

      while (fgets(line, sizeof(line), trips) != NULL) {
        int field_count = 0;
        char *cur_line = line;
        trim_line(cur_line);
        int temp_route_id;
        char *field;
        const char delim[2] = ",";

        if (count > 0) {
          while ((field = strsep(&cur_line, delim)) != NULL ) {

            switch (field_count) {
              case 0:
                temp_route_id = (int) strtol(field, NULL, 10);
                break;

              case 2: {
                if (strtol(field, NULL, 10) == trip_id) {
                  fclose(trips);
                  free(file_path);
                  return temp_route_id;
               }
              }
            }

            field_count++;
          }
        }

        count++;
      }

    } else {
      printf("Unable to read trips.txt!\n");
      exit(EXIT_FAILURE);
    }

    if (trips != NULL) {
      fclose(trips);
    }

    free(file_path);

    return 0;
}

/**
 * Get bus number or train line (string).
 */
char *get_bus_num(int route_id) {
  // Get route_id.
  char const *routes_filename = "/routes.txt";

    char line[1024];
    // Determine path to stops_times.txt
    char *file_path = malloc(strlen(FILES_DIR) + strlen(routes_filename) + 1);
    strcat(strcpy(file_path, FILES_DIR), routes_filename);
    FILE *routes = fopen(file_path, "r");

    if (routes != NULL) {
      int count = 0;

      while (fgets(line, sizeof(line), routes) != NULL) {
        int field_count = 0;
        char *cur_line = line;
        trim_line(cur_line);
        char *field;
        const char delim[2] = ",";
        int this_row = 0;

        if (count > 0) {
          while ((field = strsep(&cur_line, delim)) != NULL ) {

            switch (field_count) {
              case 0: {
                if (route_id == (int)strtol(field, NULL, 10)) {
                  this_row = 1;
                }

                break;
              }

              case 2: {
                if (this_row) {
                  fclose(routes);
                  free(file_path);
                  return field;
                }
              }
            }

            field_count++;
          }
        }

        count++;
      }

    } else {
      printf("Unable to read routes.txt!\n");
      exit(EXIT_FAILURE);
    }

    if (routes != NULL) {
      fclose(routes);
    }

    free(file_path);

    return 0;
}
